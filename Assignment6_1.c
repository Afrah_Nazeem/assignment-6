//Program to print number triangle using recursive function pattern
#include <stdio.h>
int main(){
    int n;
    printf("Number of lines : ");
    scanf ("%d",&n);
    printf ("\n");
    pattern(n,1);
    return 0;
    }
void printn (int num){
    if (num == 0)
        return ;
        printf ("%d", num);
        printn(num -1);
    }

void pattern (int n, int i)
    {
    if (n==0)
        return ;
        printn(i);
        printf("\n");
        pattern(n-1, i+1);
    }

